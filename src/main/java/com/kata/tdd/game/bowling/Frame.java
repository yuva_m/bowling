package com.kata.tdd.game.bowling;

class Frame {
    private int[] rolls;
    private int rollIndex;

    Frame() {
        rolls = new int[2];
        rollIndex = 0;
    }

    void addRoll(String roll) {
        int rollValue = getRollValue(roll);
        rolls[rollIndex] = rollValue;
        rollIndex++;
    }

    private int getRollValue(String roll) {
        int rollValue;

        if ("X".equals(roll)) {
            rollValue = 10;
        } else if ("/".equals(roll)) {
            rollValue = 10 - rolls[0];
        } else if ("-".equals(roll)) {
            rollValue = 0;
        } else {
            rollValue = Integer.valueOf(roll);
        }
        return rollValue;
    }

    boolean isFull() {
        return rollIndex == 2 || isStrike();
    }

    boolean isStrike() {
        return rolls[0] == 10;
    }

    int getFrameScore() {
        return rolls[0]+rolls[1];
    }

    boolean isSpare() {
        return getFrameScore() == 10 && rolls[0] != 10;
    }

    int getRoll1() {
        return rolls[0];
    }
}