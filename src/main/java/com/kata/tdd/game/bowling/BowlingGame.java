package com.kata.tdd.game.bowling;

import java.util.ArrayList;
import java.util.List;

class BowlingGame {
    private Frame frame;
    private final List<Frame> frames = new ArrayList<>();
    private int totalScore;

    BowlingGame() {
        frame = new Frame();
        frames.add(frame);
    }

    void addRoll(String roll) {
        frame.addRoll(roll);

        if(frame.isFull()) {
            frame = new Frame();
            frames.add(frame);
        }
    }

    private void calculateScore() {
        for(Frame frame : frames) {
            totalScore += frame.getFrameScore();
        }
    }

    String getTotalScore() {
        calculateScore();
        calculateBonus();
        return String.valueOf(totalScore);
    }

    private void calculateBonus() {
        for (int frameIndex = 0; frameIndex < frames.size(); frameIndex++) {
            Frame currentFrame = frames.get(frameIndex);

            if(currentFrame.isSpare()) {
                getSpareBonus(frameIndex);
            } else {
                if(currentFrame.isStrike()) {
                    getStrikeBonus(frameIndex);
                }
            }
        }
    }

    private void getStrikeBonus(int frameIndex) {
        Frame nextFrame;
        nextFrame = frames.get(frameIndex + 1);
        totalScore += nextFrame.getFrameScore();

        if(nextFrame.isStrike()) {
            totalScore += frames.get(frameIndex+2).getRoll1();
        }
    }

    private void getSpareBonus(int frameIndex) {
        Frame nextFrame;
        nextFrame = frames.get(frameIndex + 1);
        totalScore += nextFrame.getRoll1();
    }
}
