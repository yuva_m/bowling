package com.kata.tdd.game.bowling;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class FrameTester {
    private Frame frame;

    @Before
    public void init() {
        frame = new Frame();
    }

    @Test
    public void frameScoreShouldBe0WhenGameBegins() {
        assertThat(frame.getFrameScore(), is(0));
    }

    @Test
    public void frameScoreShouldBe5ForASingleRollOf5() {
        frame.addRoll("5");
        assertThat(frame.getFrameScore(), is(5));
    }

    @Test
    public void frameScoreShouldBe10ForAStrike() {
        frame.addRoll("X");
        assertThat(frame.getFrameScore(), is(10));
    }

    @Test
    public void frameScoreShouldBe10ForASpare() {
        frame.addRoll("4");
        frame.addRoll("/");
        assertThat(frame.getFrameScore(), is(10));
    }

    @Test
    public void frameScoreShouldNotBeChangedForAMiss() {
        frame.addRoll("4");
        frame.addRoll("-");
        assertThat(frame.getFrameScore(), is(4));
    }

    @Test
    public void isFullShouldReturnTrueForAStrikeFrame() {
        frame.addRoll("X");
        assertTrue(frame.isFull());
    }

    @Test
    public void isSpareShouldReturnTrueForASpareFrame() {
        frame.addRoll("4");
        frame.addRoll("/");
        assertTrue(frame.isSpare());
    }
}
