package com.kata.tdd.game.bowling;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class BowlingGameTester {
    private BowlingGame bowlingGame;

    @Before
    public void init() {
        bowlingGame = new BowlingGame();
    }

    @Test
    public void totalScoreShouldBe0WhenGameBegins() {
        assertThat(bowlingGame.getTotalScore(), is("0"));
    }

    @Test
    public void totalScoreShouldBe5ForASingleRollOf5() {
        bowlingGame.addRoll("5");
        assertThat(bowlingGame.getTotalScore(), is("5"));
    }

    @Test
    public void totalScoreShouldBe10ForAStrike() {
        bowlingGame.addRoll("X");
        assertThat(bowlingGame.getTotalScore(), is("10"));
    }

    @Test
    public void totalScoreShouldBe10ForASpare() {
        bowlingGame.addRoll("4");
        bowlingGame.addRoll("/");
        assertThat(bowlingGame.getTotalScore(), is("10"));
    }

    @Test
    public void totalScoreShouldNotBeChangedForAMiss() {
        bowlingGame.addRoll("4");
        bowlingGame.addRoll("-");
        assertThat(bowlingGame.getTotalScore(), is("4"));
    }

    @Test
    public void totalScoreShouldBe25For2Spares() {
        bowlingGame.addRoll("4");
        bowlingGame.addRoll("/");
        bowlingGame.addRoll("5");
        bowlingGame.addRoll("/");
        assertThat(bowlingGame.getTotalScore(), is("25"));
    }

    @Test
    public void totalScoreShouldBe37ForRollsWithScore29AndBonus3() {
        bowlingGame.addRoll("4");
        bowlingGame.addRoll("/");
        bowlingGame.addRoll("5");
        bowlingGame.addRoll("/");
        bowlingGame.addRoll("3");
        bowlingGame.addRoll("6");
        assertThat(bowlingGame.getTotalScore(), is("37"));
    }

    @Test
    public void totalScoreShouldBe34ForRollsWithScore26AndBonus8() {
        bowlingGame.addRoll("X");
        bowlingGame.addRoll("5");
        bowlingGame.addRoll("3");
        bowlingGame.addRoll("2");
        bowlingGame.addRoll("6");
        assertThat(bowlingGame.getTotalScore(), is("34"));
    }

    @Test
    public void totalScoreShouldBe59ForRollsWithScore46AndBonus8() {
        bowlingGame.addRoll("X");
        bowlingGame.addRoll("X");
        bowlingGame.addRoll("5");
        bowlingGame.addRoll("3");
        bowlingGame.addRoll("2");
        bowlingGame.addRoll("6");
        assertThat(bowlingGame.getTotalScore(), is("59"));
    }
}
